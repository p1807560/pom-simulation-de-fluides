# POM - Simulation de fluides

Ce projet à pour but de simuler des nuages en 3D, en utilisant CUDA.

Il s'inspire des travaux de Jos Stam : http://graphics.cs.cmu.edu/nsp/course/15-464/Fall09/papers/StamFluidforGames.pdf

![Exemple de résultat](screenshots/screenshot1.png "Exemple de résultat")
