#include "app.hpp"

#include <string>

int main(int argc, char *argv[])
{
    if (argc == 3)
    {
        std::string fileName = argv[1];
        float duration = std::stof(argv[2]);
        App sim(fileName, duration);
        sim.run();
    }
    else if (argc == 4)
    {
        int w, h, s;
        w = std::stoi(argv[1]);
        h = std::stoi(argv[2]);
        s = std::stoi(argv[3]);
        App sim(w, h, s);
        sim.run();
    }
    else
    {
        App sim;
        sim.run();
    }

    return 0;
}