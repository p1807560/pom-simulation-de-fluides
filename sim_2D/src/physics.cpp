#include "physics.hpp"

#include <utility>

int Physics::offset(int x, int y)
{
    return x + y * (width + 2);
}

void Physics::setWidth(int w)
{
    width = w;
}

void Physics::setScale(float s)
{
    scale = s;
}

/**
 * @brief Solve a system of linear equations using the Gauss-Seidel solver
 *
 * @param b Boundary conditions settings
 * @param x Output
 * @param x0 Initial guess
 * @param a Linear system parameter (multiplication)
 * @param c Linear system parameter (division)
 */
void Physics::lin_solve(boundaryType b, float *x, float *x0, float a, float c)
{
    for (int k = 0; k < 20; ++k)
    {
        for (int i = 1; i <= width; ++i)
        {
            for (int j = 1; j <= width; ++j)
            {
                x[offset(i, j)] = (x0[offset(i, j)] + a * (x[offset(i - 1, j)] + x[offset(i + 1, j)] + x[offset(i, j - 1)] + x[offset(i, j + 1)])) / c;
            }
        }
        setBoundaries(b, x);
    }
}

// Sets the boundary cells
void Physics::setBoundaries(boundaryType b, float *x)
{
    for (int i = 1; i <= width; ++i)
    {
        x[offset(0, i)] = (b == boundaryType::reboundHorizontal) ? -x[offset(1, i)] : x[offset(1, i)];
        x[offset(width + 1, i)] = (b == boundaryType::reboundHorizontal) ? -x[offset(width, i)] : x[offset(width, i)];
        x[offset(i, 0)] = (b == boundaryType::reboundVertical) ? -x[offset(i, 1)] : x[offset(i, 1)];
        x[offset(i, width + 1)] = (b == boundaryType::reboundVertical) ? -x[offset(i, width)] : x[offset(i, width)];
    }

    x[offset(0, 0)] = 0.5 * (x[offset(1, 0)] + x[offset(0, 1)]);
    x[offset(0, width + 1)] = 0.5 * (x[offset(1, width + 1)] + x[offset(0, width)]);
    x[offset(width + 1, 0)] = 0.5 * (x[offset(width, 0)] + x[offset(width + 1, 1)]);
    x[offset(width + 1, width + 1)] = 0.5 * (x[offset(width, width + 1)] + x[offset(width + 1, width)]);
}

/**
 * @brief Diffusion of x0
 *
 * @param b Boundary condition settings
 * @param x Output
 * @param x0 Quantity to be diffused
 * @param diff Diffusion rate
 * @param dt Time step
 */
void Physics::diffuse(boundaryType b, float *x, float *x0, float diff, float dt)
{
    float a = dt * diff * scale * scale;
    lin_solve(b, x, x0, a, 1 + 4 * a);
}

/**
 * @brief Move d0 along the velocity field
 *
 * @param b Boundary conditions settings
 * @param d Output
 * @param d0 Quantity to advect
 * @param vx x Component of the velocity field
 * @param vy y Component of the velocity field
 * @param dt Time step
 */
void Physics::advect(boundaryType b, float *d, float *d0, float *vx, float *vy, float dt)
{
    int i0, j0, i1, j1;
    float x, y, s0, t0, s1, t1;

    float dt0 = dt * scale;
    for (int i = 1; i <= width; ++i)
    {
        for (int j = 1; j <= width; ++j)
        {
            // Trace the cell backwards in time
            x = i - dt0 * vx[offset(i, j)];
            y = j - dt0 * vy[offset(i, j)];

            // Stay in the boundaries
            if (x < 0.5)
                x = 0.5;
            if (x > width + 0.5)
                x = width + 0.5;
            if (y < 0.5)
                y = 0.5;
            if (y > width + 0.5)
                y = width + 0.5;

            // Linear interpolation to over the previous density values to find the current one
            i0 = static_cast<int>(x);
            i1 = i0 + 1;
            j0 = static_cast<int>(y);
            j1 = j0 + 1;

            s1 = x - i0;
            s0 = 1 - s1;
            t1 = y - j0;
            t0 = 1 - t1;

            d[offset(i, j)] = s0 * (t0 * d0[offset(i0, j0)] + t1 * d0[offset(i0, j1)]) +
                              s1 * (t0 * d0[offset(i1, j0)] + t1 * d0[offset(i1, j1)]);
        }
    }
    setBoundaries(b, d);
}

/**
 * @brief Forces the velocities to be mass-conserving
 *
 * Idea: every velocity field is the sum of an incompressible field and a gradient field.
 *      (Hodge decomposition). We want to substract the gradient field to the velocity field
 *      in order to make the velocity field incompressible.
 *
 * @param vx
 * @param vy
 * @param p
 * @param div
 * @return __device__
 */
void Physics::project(float *vx, float *vy, float *p, float *div)
{
    int i, j;
    float h = 1.0 / scale;

    // Calculating the divergence div of the velocity field
    // For an incompressible fluid, it should be 0 everywhere,
    // but it is not the case here (for now).
    for (i = 1; i <= width; ++i)
    {
        for (j = 1; j <= width; ++j)
        {
            div[offset(i, j)] = -0.5 * h * (vx[offset(i + 1, j)] - vx[offset(i - 1, j)] + vy[offset(i, j + 1)] - vy[offset(i, j - 1)]);
            p[offset(i, j)] = 0.0;
        }
    }

    // Set boundaries
    setBoundaries(boundaryType::noRebound, div);
    setBoundaries(boundaryType::noRebound, p);

    // Solve Poisson equation to get the scalar/height field (= pressure field)
    lin_solve(boundaryType::noRebound, p, div, 1, 4);

    // The gradient of the pressure field found by solving the Poisson Equation
    // is substracted from the velocity field :
    // Only the divergence-free component remain !
    for (i = 1; i <= width; ++i)
    {
        for (j = 1; j <= width; ++j)
        {
            vx[offset(i, j)] -= 0.5 * (p[offset(i + 1, j)] - p[offset(i - 1, j)]) / h;
            vy[offset(i, j)] -= 0.5 * (p[offset(i, j + 1)] - p[offset(i, j - 1)]) / h;
        }
    }

    // Set boundaries
    setBoundaries(boundaryType::reboundHorizontal, vx);
    setBoundaries(boundaryType::reboundVertical, vy);
}


// Density solver
// Input: x0[] contains the source densities
// Output: x[]
void Physics::dens_step(float *x, float *x0, float *vx, float *vy, float diff, float dt)
{
    // add_source(N, x, x0, dt);
    std::swap(x0, x);
    diffuse(boundaryType::noRebound, x, x0, diff, dt);
    std::swap(x0, x);
    advect(boundaryType::noRebound, x, x0, vx, vy, dt);
}

// Velocity solver
// Input: u0[] and v0[] contain the source velocities
// Output: vx[] and vy[]
void Physics::vel_step(float *vx, float *vy, float *u0, float *v0, float visc, float dt)
{
    std::swap(u0, vx);
    std::swap(v0, vy);
    // Viscous diffusion
    diffuse(boundaryType::reboundHorizontal, vx, u0, visc, dt);
    diffuse(boundaryType::reboundVertical, vy, v0, visc, dt);
    project(vx, vy, u0, v0);
    std::swap(u0, vx);
    std::swap(v0, vy);
    // Self-advection
    advect(boundaryType::reboundHorizontal, vx, u0, u0, v0, dt);
    advect(boundaryType::reboundVertical, vy, v0, u0, v0, dt);
    project(vx, vy, u0, v0);
}

void Physics::fade_density(float *dens, float dt)
{
    float d;
    for (int x = 0; x < width; ++x)
    {
        for (int y = 0; y < width; ++y)
        {
            d = dens[offset(x, y)];
            dens[offset(x, y)] = (d - 0.07f < 0) ? 0 : d - 0.07f;
        }
    }
}