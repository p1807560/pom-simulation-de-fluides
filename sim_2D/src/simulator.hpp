#ifndef __SIMULATOR_H__
#define __SIMULATOR_H__

#include <string>

class Simulator
{
public:
    Simulator(int width, int height, float scale, float visc = 0.001, float diff = 300.f);
    Simulator(const std::string& fileName);
    ~Simulator();

    void step(double dt);

    void addDensity(int x, int y, float dens, float dt);
    void addVelocity(int x, int y, float vx, float vy, float dt);

    int getDensity(int x, int y);
    float getVelocityX(int x, int y);
    float getVelocityY(int x, int y);

    void save() const;

private:
    // Mapping a 2D array to a 1D array
    int offset(int x, int y);

private:
    int width;
    int height;
    float scale;

    // Velocity
    float *vx;
    float *vy;

    float *vx_prev;
    float *vy_prev;

    // Density
    float *dens;
    float *dens_prev;

    // Viscosity
    float visc;

    float diff;
};

#endif // __SIMULATOR_H__
