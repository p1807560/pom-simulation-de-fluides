#ifndef __PHYSICS_H__
#define __PHYSICS_H__

namespace Physics
{
    // =========== Set-Up ===========
    static int width; // grid width
    static float scale;
    void setWidth(int w);
    void setScale(float s);

    // Behaviour for boundary conditions
    enum class boundaryType
    {
        reboundVertical,   // Rebound on vertical boundaries
        reboundHorizontal, // Rebound on horizontal boundaries
        noRebound
    };

    // =========== Main Steps ===========
    void vel_step(float *vx, float *vy, float *u0, float *v0, float visc, float dt);
    void dens_step(float *x, float *x0, float *vx, float *vy, float diff, float dt);
    void fade_density(float *dens, float dt);

    // =========== Intermediate Computations ===========
    // Linear equation solver (using Gauss-Seidel Relaxation)
    void lin_solve(boundaryType b, float *x, float *x0, float a, float c);

    // Sets the boundary cells
    void setBoundaries(boundaryType b, float *x);

    void diffuse(boundaryType b, float *x, float *x0, float diff, float dt);

    void advect(boundaryType b, float *d, float *d0, float *vx, float *vy, float dt);

    // Force the velocity field to be mass conserving
    void project(float *vx, float *vy, float *p, float *div);
    // =========== Utilities ===========
    int offset(int x, int y);

}

#endif // __PHYSICS_H__
