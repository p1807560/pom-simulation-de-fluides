#include "simulator.hpp"
#include <utility>
#include <iostream>
#include <sstream>
#include <cassert>
#include <fstream>

#include "physics.hpp"

int Simulator::offset(int x, int y)
{
    return x + y * (width + 2);
}

Simulator::Simulator(int width, int height, float scale, float visc, float diff)
    : width(width),
      height(height),
      scale(scale),
      visc(visc),
      diff(diff)
{
    assert(width == height);

    vx = new float[(width + 2) * (height + 2)];
    vx_prev = new float[(width + 2) * (height + 2)];

    vy = new float[(width + 2) * (height + 2)];
    vy_prev = new float[(width + 2) * (height + 2)];

    // Density
    dens = new float[(width + 2) * (height + 2)];
    dens_prev = new float[(width + 2) * (height + 2)];

    for (int x = 0; x < (width + 2) * (height + 2); ++x)
    {
        vx[x] = 0.f;
        vy[x] = 0.f;
        dens[x] = 0.f;

        vx_prev[x] = 0.f;
        vy_prev[x] = 0.f;
        dens_prev[x] = 0.f;
    }
    Physics::setWidth(width);
    Physics::setScale(scale);
}

Simulator::Simulator(const std::string &fileName)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open())
    {
        std::cout << "Error: could not open file" << std::endl;
        exit(1);
    }
    int s;
    file >> width >> height >> s;
    scale = 1.f / float(s);

    file >> visc >> diff;
    
    vx = new float[(width + 2) * (height + 2)];
    vx_prev = new float[(width + 2) * (height + 2)];

    vy = new float[(width + 2) * (height + 2)];
    vy_prev = new float[(width + 2) * (height + 2)];

    // Density
    dens = new float[(width + 2) * (height + 2)];
    dens_prev = new float[(width + 2) * (height + 2)];

    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file >> dens[i];

        vx_prev[i] = 0.f;
        vy_prev[i] = 0.f;
        dens_prev[i] = 0.f;
    }

    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file >> vx[i];
    }

    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file >> vy[i];
    }

    file.close();

    Physics::setWidth(width);
    Physics::setScale(scale);
}

Simulator::~Simulator()
{
    delete[] vx;
    delete[] vx_prev;
    delete[] vy;
    delete[] vy_prev;
    delete[] dens;
    delete[] dens_prev;
}

int Simulator::getDensity(int x, int y)
{
    return dens[offset(x + 1, y + 1)];
}
float Simulator::getVelocityX(int x, int y)
{
    return vx[offset(x, y)];
}
float Simulator::getVelocityY(int x, int y)
{
    return vy[offset(x, y)];
}

void Simulator::addDensity(int x, int y, float densValue, float dt)
{
    dens[offset(x, y)] += densValue * dt;
    dens[offset(x, y)] /= 1.0f + dt;
}

void Simulator::addVelocity(int x, int y, float uValue, float vValue, float dt)
{
    vx[offset(x, y)] += uValue * dt;
    vx[offset(x, y)] /= 1.0f + dt;
    vy[offset(x, y)] += vValue * dt;
    vy[offset(x, y)] /= 1.0f + dt;
}

void Simulator::step(double dt)
{
    Physics::vel_step(vx, vy, vx_prev, vy_prev, visc, dt);
    Physics::dens_step(dens, dens_prev, vx, vy, diff, dt);
}

void Simulator::save() const
{
    std::ofstream file;
    std::stringstream ss;
    std::string fileName;

    int s = int(1.f / scale);

    ss << "../data/" << width * s << "-" << s << ".txt";
    ss >> fileName;

    file.open(fileName);
    if (!file.is_open())
    {
        std::cout << "Error: could not open file" << std::endl;
        exit(1);
    }

    file << width << " " << height << " " << s << std::endl;
    file << visc << " " << diff << std::endl;
    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file << dens[i] << " ";
    }
    file << std::endl;
    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file << vx[i] << " ";
    }
    file << std::endl;
    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file << vy[i] << " ";
    }
    file << std::endl;

    file.close();
}