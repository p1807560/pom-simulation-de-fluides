#ifndef __SHADER_HPP__
#define __SHADER_HPP__

#include <GL/glew.h>

#include <string>

/**
 * @brief Encapsulates a shader program
 *
 */
class Shader
{
public:
    GLuint program; // Program ID
    bool isInit;

    // Reads and builds the shader program
    Shader(const std::string &vertexShaderPath, const std::string &fragmentShaderPath);
    Shader();

    Shader(const Shader& rhs) = delete;
    Shader(Shader&& rhs);
    Shader& operator=(const Shader& rhs) = delete;
    Shader& operator=(Shader&& rhs);
    ~Shader();

    // Activate / Use the program
    void use();

    // Uniform utility function
    void setUniform(const std::string &name, bool value) const;
    void setUniform(const std::string &name, int value) const;
    void setUniform(const std::string &name, float value) const;
    void setUniform(const std::string &name, float v1, float v2, float v3, float v4) const;

private:
    std::string readFile(std::string fileName);

    void compileShader(GLuint shaderType, GLuint shader);
    GLuint loadShader(GLuint shaderType, std::string shaderPath);
    void linkProgram(GLuint program, GLuint vertexShader, GLuint fragmentShader);
};

#endif // __SHADER_HPP__
