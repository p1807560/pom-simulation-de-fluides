#include "cuda_runtime.h"

void kernel_setWidth(uint width);
void kernel_setScale(float scale);
void kernel_setPtrs(float* dens, float* vx, float* vy);

void launchKernel(uint blockSize, double dt, float *vx, float *vy,
                  float *vx_prev, float *vy_prev,
                  float *dens, float *dens_prev,
                  float visc, float diff, uint width,
                  unsigned int* texture_out);

void addDensity(int px, int py, float val, float dt);
void addVelocity(int px, int py, float valX, float valY, float dt);