#include "cudaGLRenderer.hpp"

#include <GL/glew.h>
#include <iostream>

#include "libs/helper_gl.h"
#include "libs/helper_cuda.h"

CudaGLRenderer::CudaGLRenderer(uint width, uint height, float duration)
    : width(width),
      height(height),
      duration(duration)
{
    initSDL();
    initGL();

    createGLTextureForCUDA(&opengl_tex_cuda, &cuda_tex_resource, width, height);
    SDK_CHECK_ERROR_GL();
    initCUDABuffers();

    shaderProgram = Shader("shaders/vertexShader.glsl", "shaders/fragmentShader.glsl");
    setupGL();
}

void CudaGLRenderer::init(uint _width, uint _height, float _duration)
{
    width = _width,
    height = _height;
    duration = _duration;
    initSDL();
    initGL();

    createGLTextureForCUDA(&opengl_tex_cuda, &cuda_tex_resource, width, height);
    SDK_CHECK_ERROR_GL();
    initCUDABuffers();

    shaderProgram = Shader("shaders/vertexShader.glsl", "shaders/fragmentShader.glsl");
    setupGL();
}

CudaGLRenderer::~CudaGLRenderer()
{
    // Free OpenGL
    glDeleteBuffers(1, &ebo);
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);

    // cudaGraphicsGL
    cudaGraphicsUnregisterResource(cuda_tex_resource);
    cudaFree(cuda_dev_render_buffer);

    // Free SDL
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void CudaGLRenderer::initSDL()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Error: Could not initalize SDL...";
        exit(1);
    }

    // Use OpenGL core
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    window = SDL_CreateWindow("2D Fluid Simulation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              width, height, SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        std::cout << "Error: could not create window...";
        exit(1);
    }

    glContext = SDL_GL_CreateContext(window);
    if (!glContext)
    {
        std::cout << "Error: [GL] failed to get GL context from window" << std::endl;
        exit(1);
    }
    SDL_GL_SetSwapInterval(0);
}

void CudaGLRenderer::initGL()
{
    glewExperimental = GL_TRUE; // need this to enforce core profile
    GLenum err = glewInit();
    glGetError(); // parse first error
    if (err != GLEW_OK)
    { // Problem: glewInit failed, something is seriously wrong.
        printf("glewInit failed: %s \n", glewGetErrorString(err));
        exit(1);
    }
}

// Create 2D OpenGL texture in gl_tex and bind it to CUDA in cuda_tex
void CudaGLRenderer::createGLTextureForCUDA(GLuint *gl_tex, cudaGraphicsResource **cuda_tex, unsigned int size_x, unsigned int size_y)
{
    // create an OpenGL texture
    glGenTextures(1, gl_tex);              // generate 1 texture
    glBindTexture(GL_TEXTURE_2D, *gl_tex); // set it as current target
    // set basic texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // clamp s coordinate
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // clamp t coordinate
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // Specify 2D texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8UI_EXT, size_x, size_y, 0, GL_RGBA_INTEGER_EXT, GL_UNSIGNED_BYTE, NULL);
    // Register this texture with CUDA
    checkCudaErrors(cudaGraphicsGLRegisterImage(cuda_tex, *gl_tex, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsWriteDiscard));
    SDK_CHECK_ERROR_GL();
}

void CudaGLRenderer::initCUDABuffers()
{
    // set up vertex data parameters
    uint num_texels = width * height;
    uint num_values = num_texels * 4;
    size_t size_tex_data = sizeof(GLubyte) * num_values;
    // We don't want to use cudaMallocManaged here - since we definitely want
    checkCudaErrors(cudaMalloc(&cuda_dev_render_buffer, size_tex_data)); // Allocate CUDA memory for color output
}

void CudaGLRenderer::setupGL()
{
    glViewport(0, 0, width, height); // viewport for x,y to normalized device coordinates transformation
    SDK_CHECK_ERROR_GL();

    // QUAD GEOMETRY
    GLfloat vertices[] = {
        // Positions      // Texture Coords
        1.0f, 1.0f, 0.5f, 1.0f, 1.0f,   // Top Right
        1.0f, -1.0f, 0.5f, 1.0f, 0.0f,  // Bottom Right
        -1.0f, -1.0f, 0.5f, 0.0f, 0.0f, // Bottom Left
        -1.0f, 1.0f, 0.5f, 0.0f, 1.0f   // Top Left
    };
    // you can also put positions, colors and coordinates in seperate VBO's
    GLuint indices[] = {
        // Note that we start from 0!
        0, 1, 3, // First Triangle
        1, 2, 3  // Second Triangle
    };

    // Generate buffers
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    // Buffer setup
    // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
    glBindVertexArray(vao); // all next calls wil use this VAO (descriptor for VBO)

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute (3 floats)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid *)0);
    glEnableVertexAttribArray(0);
    // Texture attribute (2 floats)
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid *)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound
    // vertex buffer object so afterwards we can safely unbind
    glBindVertexArray(0);
}

void CudaGLRenderer::generateCudaImage(float dt)
{
    launchCuda(dt);

    // We want to copy cuda_dev_render_buffer data to the texture
    // Map buffer objects to get CUDA device pointers
    cudaArray *texture_ptr;
    checkCudaErrors(cudaGraphicsMapResources(1, &cuda_tex_resource, 0));
    checkCudaErrors(cudaGraphicsSubResourceGetMappedArray(&texture_ptr, cuda_tex_resource, 0, 0));

    int num_texels = width * height;
    int num_values = num_texels * 4;
    int size_tex_data = sizeof(GLubyte) * num_values;
    checkCudaErrors(cudaMemcpyToArray(texture_ptr, 0, 0, cuda_dev_render_buffer, size_tex_data, cudaMemcpyDeviceToDevice));
    checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_tex_resource, 0));
}

void CudaGLRenderer::display()
{
    SDK_CHECK_ERROR_GL();
    // Clear the color buffer
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    SDK_CHECK_ERROR_GL();

    glActiveTexture(GL_TEXTURE0);
    SDK_CHECK_ERROR_GL();
    glBindTexture(GL_TEXTURE_2D, opengl_tex_cuda);
    SDK_CHECK_ERROR_GL();

    shaderProgram.use(); // we gonna use this compiled GLSL program
    shaderProgram.setUniform("tex", 0);
    SDK_CHECK_ERROR_GL();

    glBindVertexArray(vao); // binding VAO automatically binds EBO
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0); // unbind VAO

    SDK_CHECK_ERROR_GL();

    SDL_GL_SwapWindow(window);
}

void CudaGLRenderer::run()
{
    std::chrono::time_point<std::chrono::steady_clock> time, time_prev;
    std::chrono::duration<double, std::milli> diff;
    bool running = true;

    time_prev = std::chrono::steady_clock::now();
    bool leftClick = false;
    float totalTime = 0.f;
    int nbFrames = 0;
    while (running)
    {
        time = std::chrono::steady_clock::now();
        diff = time - time_prev;
        double dt = diff.count();
        dt /= 1000.0; // Convert dt from ms to s
        totalTime += dt;
        if (duration > 0 && totalTime >= duration)
            break;
        nbFrames++;

        bool save = false;
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                running = false;
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
                running = false;
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_SPACE)
                save = true;
            else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
            {
                leftClick = true;
            }
            else if (event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_LEFT)
            {
                leftClick = false;
            }
        }

        update(dt, leftClick, save);
        generateCudaImage(dt);
        display();

        std::swap(time, time_prev);
    }
    std::cout << "Performances for " << totalTime << "s of simulation: " << totalTime / float(nbFrames) << "s per frame, or " 
              << 1 / (totalTime / float(nbFrames)) << " fps on avereage" << std::endl;
}