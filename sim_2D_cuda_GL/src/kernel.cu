#include "cuda_runtime.h"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>

#include "kernel.cuh"
#include "physics.cuh"

void kernel_setWidth(uint width)
{
	Physics::setWidth<<<1, 1>>>(width);
}

void kernel_setScale(float scale)
{
	Physics::setScale<<<1, 1>>>(scale);
}

void launchKernel(uint blockSize, double dt, float *vx, float *vy,
				  float *vx_prev, float *vy_prev,
				  float *dens, float *dens_prev,
				  float visc, float diff, uint width,
				  unsigned int *texture_out)
{
	// Kernel arguments
	void *args[] = {
		(void *)&dt,
		(void *)&vx,
		(void *)&vy,
		(void *)&vx_prev,
		(void *)&vy_prev,
		(void *)&dens,
		(void *)&dens_prev,
		(void *)&visc,
		(void *)&diff,
		(void *)&texture_out,
	};

	// Grid and block dimentsions
	// const unsigned int blockSize = 32;
	dim3 blockDim(blockSize, blockSize, 1);
	unsigned int gridSize = (width + 2) % blockSize == 0 ? (width + 2) / blockSize : (width + 2) / blockSize + 1;

	// Check available executions nb
	int numBlocksPerSm = 0;
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, 0);
	cudaOccupancyMaxActiveBlocksPerMultiprocessor(&numBlocksPerSm, Physics::cudaStep, blockSize * blockSize, 0);
	uint maxExec = deviceProp.multiProcessorCount * numBlocksPerSm;

	if (gridSize * gridSize > maxExec)
	{
		gridSize = floor(sqrt(maxExec));
	}
	// std::cout << "Max Executions in Grid: " << blockSize << std::endl;

	dim3 gridDim(gridSize, gridSize, 1);

	// Launch kernel
	cudaLaunchCooperativeKernel((void *)Physics::cudaStep, gridDim, blockDim, args);
	cudaDeviceSynchronize();
}

void addDensity(int px, int py, float val, float dt)
{
	Physics::addDensity<<<1, 1>>>(px, py, val, dt);
}

void addVelocity(int px, int py, float valX, float valY, float dt)
{
	Physics::addVelocity<<<1, 1>>>(px, py, valX, valY, dt);
}


void kernel_setPtrs(float* dens, float* vx, float* vy)
{
	Physics::setPtrs<<<1, 1>>>(dens, vx, vy);
}