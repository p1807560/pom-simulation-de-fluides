#ifndef __PHYSICS_H__
#define __PHYSICS_H__

namespace Physics
{
    // =========== Set-Up ===========
    __device__ static int width; // grid width
    __device__ static float scale;
    __global__ void setWidth(int w);
    __global__ void setScale(float s);

    __device__ static float *dens = nullptr;
    __device__ static float *vx = nullptr;
    __device__ static float *vy = nullptr;
    __global__ void setPtrs(float *dens, float *vx, float *vy);

    // Behaviour for boundary conditions
    enum class boundaryType
    {
        reboundVertical,   // Rebound on vertical boundaries
        reboundHorizontal, // Rebound on horizontal boundaries
        noRebound
    };

    // =========== Interface ===========
    __global__ void cudaStep(double dt, float *vx, float *vy,
                             float *vx_prev, float *vy_prev,
                             float *dens, float *dens_prev,
                             float visc, float diff,
                             unsigned int *texture_out);

    __global__ void addDensity(int px, int py, float val, float dt);
    __global__ void addVelocity(int px, int py, float valX, float valY, float dt);

    // =========== Main Steps ===========
    __device__ void vel_step(float *vx, float *vy, float *u0, float *v0, float visc, float dt);
    __device__ void dens_step(float *x, float *x0, float *vx, float *vy, float diff, float dt);
    __device__ void fade_density(float *dens, float dt);
    __device__ void fill_texture(float *dens, unsigned int *texture_out);

    // =========== Intermediate Computations ===========
    // Linear equation solver (using Gauss-Seidel Relaxation)
    __device__ void lin_solve(boundaryType b, float *x, float *x0, float a, float c);

    // Sets the boundary cells
    __device__ void setBoundaries(boundaryType b, float *x);

    __device__ void diffuse(boundaryType b, float *x, float *x0, float diff, float dt);

    __device__ void advect(boundaryType b, float *d, float *d0, float *vx, float *vy, float dt);

    // Force the velocity field to be mass conserving
    __device__ void project(float *vx, float *vy, float *p, float *div);

    // =========== Utilities ===========
    // map 2D array to 1D
    __device__ int offset(int x, int y);

    // std::swap does not exist in CUDA
    __device__ void swap(float *&pf1, float *&pf2);

    // Compute grid coordinates using thread / block information
    __device__ int getIndexX();
    __device__ int getIndexY();

    __device__ float clamp(float x, float a, float b);
    __device__ int rgbToInt(float r, float g, float b);
}

#endif // __PHYSICS_H__
