#include "app.hpp"

#include <string>
#include <iostream>

int main(int argc, char *argv[])
{
    if (argc == 4)
    {
        std::string fileName = argv[1];
        float duration = std::stof(argv[2]);
        int blockSize = std::stoi(argv[3]);
        App sim(blockSize, fileName, duration);
        sim.run();
    }
    else if (argc == 5)
    {
        int w, h, s;
        w = std::stoi(argv[1]);
        h = std::stoi(argv[2]);
        s = std::stoi(argv[3]);
        int blockSize = std::stoi(argv[4]);

        App sim(blockSize, w, h, s);
        sim.run();
    }
    else
    {
        App sim;
        sim.run();
    }

    return 0;
}