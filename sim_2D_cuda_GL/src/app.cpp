#include "app.hpp"

#include "cuda_runtime.h"
#include "libs/helper_cuda.h"

#include "kernel.cuh"

#include <iostream>
#include <sstream>
#include <cassert>

App::App(uint blockSize, uint width, uint height, uint scale, float visc, float diff)
    : CudaGLRenderer(width, height),
      size((width / scale + 2) * (height / scale + 2)),
      scale(scale),
      visc(visc),
      diff(diff),
      inputAllowed(true),
      duration(-1.f),
      blockSize(blockSize)
{
    checkCudaErrors(cudaMalloc((void **)&vx, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vx_prev, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy_prev, size * sizeof(float)));

    // Density
    checkCudaErrors(cudaMalloc((void **)&dens, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&dens_prev, size * sizeof(float)));

    dens_interface = new float[size];
    vx_interface = new float[size];
    vy_interface = new float[size];

    float *init = new float[size];

    for (uint x = 0; x < size; ++x)
    {
        init[x] = 0.f;
        dens_interface[x] = 0.f;
        vx_interface[x] = 0.f;
        vy_interface[x] = 0.f;
    }
    checkCudaErrors(cudaMemcpy(vx, dens_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vx_prev, dens_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy, dens_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy_prev, dens_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    delete[] init;

    kernel_setWidth(int(width / scale));
    kernel_setScale(1.f / float(scale));
    kernel_setPtrs(dens, vx, vy);

    SDL_GetMouseState(&mousePrevX, &mousePrevY);

    leftClick = false;
}

App::App(uint blockSize, const std::string &fileName, float duration)
    : CudaGLRenderer(),
      inputAllowed(false),
      duration(duration),
      blockSize(blockSize)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open())
    {
        std::cout << "Error: could not open file" << std::endl;
        exit(1);
    }

    int w, h;
    file >> w >> h >> scale;
    file >> visc >> diff;

    int width = w * scale;
    int height = h * scale;

    init(width, height, duration);

    size = (w + 2) * (h + 2);

    assert(width == height);

    std::cout << "Width : " << width << ", Height: " << height << std::endl;
    std::cout << "simulator width : " << width << std::endl;
    checkCudaErrors(cudaMalloc((void **)&vx, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vx_prev, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy_prev, size * sizeof(float)));

    // Density
    checkCudaErrors(cudaMalloc((void **)&dens, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&dens_prev, size * sizeof(float)));

    dens_interface = new float[size];
    vx_interface = new float[size];
    vy_interface = new float[size];

    float *init = new float[size];

    for (uint x = 0; x < size; ++x)
    {
        init[x] = 0.f;
    }

    // Get input from file
    for (uint i = 0; i < size; ++i)
    {
        file >> dens_interface[i];
    }

    for (uint i = 0; i < size; ++i)
    {
        file >> vx_interface[i];
    }

    for (uint i = 0; i < size; ++i)
    {
        file >> vy_interface[i];
    }

    checkCudaErrors(cudaMemcpy(vx, vx_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vx_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy, vy_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens, dens_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    delete[] init;

    kernel_setWidth(w);
    kernel_setScale(1.f / float(scale));
    kernel_setPtrs(dens, vx, vy);

    file.close();
}

App::~App()
{
    delete[] dens_interface;
    delete[] vx_interface;
    delete[] vy_interface;
    cudaFree((void *)vx);
    cudaFree((void *)vx_prev);
    cudaFree((void *)vy);
    cudaFree((void *)vy_prev);
    cudaFree((void *)dens);
    cudaFree((void *)dens_prev);
}

void App::launchCuda(float dt)
{
    launchKernel(blockSize, dt, vx, vy, vx_prev, vy_prev, dens, dens_prev, visc, diff, int(width / scale),
                 (unsigned int *)CudaGLRenderer::cuda_dev_render_buffer);
}

void App::update(float dt, bool isHoldingLeftClick, bool save)
{
    int mouseX, mouseY;
    SDL_GetMouseState(&mouseX, &mouseY);

    if (isHoldingLeftClick && inputAllowed)
    {
        float val = 8000.f * (1.f / float(scale));
        addDensity(mouseX, mouseY, val, dt);
    }

    if (inputAllowed)
    {
        // addVelocity
        int du = mouseX - mousePrevX;
        int dv = mouseY - mousePrevY;
        float fact = 3 * (1.f / float(scale)) * (1.f / dt);
        addVelocity(mouseX, mouseY, float(du) * fact, float(dv) * fact, dt);
    }

    std::swap(mouseX, mousePrevX);
    std::swap(mouseY, mousePrevY);

    if (save && inputAllowed)
    {
        saveState();
    }
}

void App::saveState()
{
    checkCudaErrors(cudaMemcpy(vx_interface, vx, size * sizeof(float), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(vy_interface, vy, size * sizeof(float), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(dens_interface, dens, size * sizeof(float), cudaMemcpyDeviceToHost));

    std::stringstream ss;
    ss << "../data/" << width << "-" << scale << ".txt";
    std::string fileName;
    ss >> fileName;

    std::ofstream file;
    file.open(fileName);
    if (!file.is_open())
    {
        std::cout << "Error: could not open file" << std::endl;
        exit(1);
    }

    int w = width / scale;
    int h = height / scale;

    for (int i = 0; i < w + 2; ++i)
    {
        for (int j = 0; j < h + 2; ++j)
        {
            std::swap(dens_interface[i * scale + (width + 1 - j)], dens_interface[i * scale + j]);
            std::swap(vx_interface[i * scale + (width + 1 - j)], vx_interface[i * scale + j]);
            std::swap(vy_interface[i * scale + (width + 1 - j)], vy_interface[i * scale + j]);
        }
    }

    file << w << " " << h << " " << scale << std::endl;
    file << visc << " " << diff << std::endl;

    for (int i = 0; i < (w + 2) * (h + 2); ++i)
    {
        file << dens_interface[i] << " ";
    }
    file << std::endl;
    for (int i = 0; i < (w + 2) * (h + 2); ++i)
    {
        file << vx_interface[i] << " ";
    }
    file << std::endl;
    for (int i = 0; i < (w + 2) * (h + 2); ++i)
    {
        file << vy_interface[i] << " ";
    }
    file << std::endl;

    file.close();
}