#ifndef __APP_HPP__
#define __APP_HPP__

#include "cudaGLRenderer.hpp"

class App : public CudaGLRenderer
{
public:
    App(uint blockSize = 32, uint width = 512, uint height = 512, uint scale = 8,
        float visc = 0.001f, float diff = 300.f);
    App(uint blockSize, const std::string& fileName, float duration);  // For performance testing (duration in secs)
    ~App();

private:
    void launchCuda(float dt) override;
    void update(float dt, bool isHoldingLeftClick, bool save) override;

    void saveState();

private:
    int mousePrevX;
    int mousePrevY;
    bool leftClick;
    
    unsigned int size;
    unsigned int scale;

    float visc;
    float diff;

    // Velocity
    float *vx; // Device memory
    float *vy; // Device memory

    float *vx_prev; // Device memory
    float *vy_prev; // Device memory

    // Density
    float *dens;      // Device memory
    float *dens_prev; // Device memory

    float *dens_interface; // Host memory
    float *vx_interface;   // Host memory
    float *vy_interface;   // Host memory

    bool inputAllowed;
    float duration;

    const uint blockSize;
};

#endif // __APP_HPP__
