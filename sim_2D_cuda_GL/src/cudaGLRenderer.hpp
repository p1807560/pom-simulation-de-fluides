#ifndef __CUDAGLRENDERER_HPP__
#define __CUDAGLRENDERER_HPP__

#include <SDL2/SDL.h>
#include <GL/glew.h>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include <chrono>

#include "shader.hpp"

class CudaGLRenderer
{
public:
    CudaGLRenderer(uint width, uint height, float duration = -1);
    CudaGLRenderer() = default;
    void init(uint _width, uint _height, float _duration = -1);
    virtual ~CudaGLRenderer();

    void run();

protected:
    void initSDL();
    void initGL();

    void createGLTextureForCUDA(GLuint *gl_tex, cudaGraphicsResource **cuda_tex,
                                unsigned int size_x, unsigned int size_y);
    void initCUDABuffers();

    void setupGL();

    void generateCudaImage(float dt);
    virtual void launchCuda(float dt) = 0;

    virtual void update(float dt, bool isHoldingLeftClick, bool save) = 0;
    void display();
   
protected:
    uint width;
    uint height;

    SDL_Window *window;
    SDL_GLContext glContext;

    Shader shaderProgram;

    void *cuda_dev_render_buffer; // Cuda buffer for initial render
    struct cudaGraphicsResource *cuda_tex_resource;
    GLuint opengl_tex_cuda; // OpenGL Texture for cuda result

    GLuint vao;
    GLuint vbo;
    GLuint ebo;

    float duration;
};

#endif // __CUDAGLRENDERER_HPP__
