#include "shader.hpp"

#include <iostream>
#include <fstream>

Shader::Shader(const std::string &vertexShaderPath, const std::string &fragmentShaderPath)
{
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, vertexShaderPath);
    GLuint fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragmentShaderPath);

    program = glCreateProgram();
    linkProgram(program, vertexShader, fragmentShader);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    isInit = true;
}

Shader::Shader()
{
    isInit = false;
}

Shader::Shader(Shader&& rhs)
{
    program = std::move(rhs.program);
    isInit = std::move(rhs.isInit);
    
    rhs.isInit = false;
}

Shader& Shader::operator=(Shader&& rhs)
{
    if (isInit)
        glDeleteProgram(program);

    program = std::move(rhs.program);
    isInit = std::move(rhs.isInit);

    rhs.isInit = false;

    return *this;
}

Shader::~Shader()
{
    if (isInit)
        glDeleteProgram(program);
}

std::string Shader::readFile(std::string fileName)
{
    std::ifstream file;
    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    std::string content;

    try
    {
        file.open(fileName);
        content = std::string((std::istreambuf_iterator<char>(file)),
                              (std::istreambuf_iterator<char>()));
    }
    catch (const std::ifstream::failure &e)
    {
        std::cerr << "ERROR::SHADER::FILE_NOT_SUCESSFULLY_READ" << std::endl;
        exit(1);
    }

    return content;
}

void Shader::compileShader(GLuint shaderType, GLuint shader)
{
    glCompileShader(shader);

    int success = true;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, 512, nullptr, infoLog);
        std::string type;
        switch (shaderType)
        {
        case GL_VERTEX_SHADER:
            type = "VERTEX";
            break;
        case GL_FRAGMENT_SHADER:
            type = "FRAGMENT";
            break;
        }
        std::cout << "ERROR::" << type << "::COMPILATION_FAILED" << std::endl
                  << infoLog << std::endl;
        exit(1);
    }
}

GLuint Shader::loadShader(GLuint shaderType, std::string shaderPath)
{
    std::string source = readFile(shaderPath);
    const char *vertexShaderSource = source.c_str();

    GLuint shader;
    shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &vertexShaderSource, NULL);
    compileShader(shaderType, shader);
    return shader;
}

void Shader::linkProgram(GLuint program, GLuint vertexShader, GLuint fragmentShader)
{
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE)
    {
        char infoLog[GL_INFO_LOG_LENGTH];
        GLint length;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
        glGetProgramInfoLog(program, length, &length, infoLog);
        std::cout << "ERROR::SHADER::LINKING_FAILED" << std::endl
                  << infoLog << std::endl;
        exit(1);
    }
}

void Shader::use()
{
    glUseProgram(program);
}

// Uniform utility functions
void Shader::setUniform(const std::string &name, bool value) const
{
    GLint location = glGetUniformLocation(program, name.c_str());
    glUniform1i(location, static_cast<int>(value));
}
void Shader::setUniform(const std::string &name, int value) const
{
    GLint location = glGetUniformLocation(program, name.c_str());
    glUniform1i(location, value);
}
void Shader::setUniform(const std::string &name, float value) const
{
    GLint location = glGetUniformLocation(program, name.c_str());
    glUniform1f(location, value);
}
void Shader::setUniform(const std::string &name, float v1, float v2, float v3, float v4) const
{
    GLint location = glGetUniformLocation(program, name.c_str());

    glUniform4f(location, v1, v2, v3, v4);
}