# Simulateur de fluide en 2D    

Ce simulateur simule des fluides en 2 dimensions.

Cette version est parallélisée avec CUDA, et l'affichage est géré par SDL2 (pas d'OpenGL)

## Prérequis:

    - Linux
    - Librairie SDL2
    - CUDA (version 11.6 utilisée)
    - pkg-config

## Build:

Pour compiler, lancer `make` dans le répertore *sim_2D*

L'exécutable s'appelle `main.out`

## Usage

`./main.out <window_width> <window_height> <cell_size> <cuda_thread_block_size>`

`./main.out`: window_width = 512, cell_size = 8

`./main.out <file> <duration> <cuda_thread_block_size>`: chargement d'un état sauvegardé, et simuler pendant duration secondes (pour les tests, les controles sont désactivés)

Exemple: `./main.out 512 512 2 16`

## Commandes 

Espace: sauvegarde de l'état (dans `../data/<window_width>-<cell_size>.txt`)

Clique gauche: ajouter de la densité

Mouvement de la souris: ajouter de la vélocité