#ifndef __APP_H__
#define __APP_H__

#include <SDL2/SDL.h>
#include <chrono>
#include <string>
#include "simulator.cuh"

class App
{
public:
    App(uint blockSize = 32, int width = 512, int height = 512, int particleSize = 8);
    App(uint blockSize, const std::string& fileName, float duration);  // For performance testing (duration in secs)
    ~App();

    void run();

private:
    void drawDensity();

private:
    SDL_Window *window;
    SDL_Renderer *renderer;

    Simulator sim;

    int width;
    int height;
    int particleSize;

    bool inputAllowed;
    float duration;
};

#endif // __APP_H__
