#include "app.hpp"
#include "iostream"
#include <ratio>
#include <cassert>
#include <fstream>

App::App(uint blockSize, int width, int height, int particleSize)
    : sim(blockSize, static_cast<int>(width / particleSize), static_cast<int>(height / particleSize), 1.f / float(particleSize)),
      width(width),
      height(height),
      particleSize(particleSize),
      inputAllowed(true),
      duration(-1.f)
{
    assert(width % particleSize == 0);
    std::cout << "App Width : " << width << ", App Height : " << height << std::endl;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Error: Could not initalize SDL...";
        exit(1);
    }

    window = SDL_CreateWindow("2D Fluid Simulation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
    if (window == nullptr)
    {
        std::cout << "Error: could not create window...";
        exit(1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        std::cout << "Error: could not create renderer...";
        exit(1);
    }
}

App::App(uint blockSize, const std::string &fileName, float duration)
    : sim(blockSize, fileName),
      inputAllowed(false),
      duration(duration)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open())
    {
        std::cout << "Error: could not open file" << std::endl;
        exit(1);
    }

    int w, h, s;
    file >> w >> h >> s;
    file.close();

    width = w * s;
    height = h * s;
    particleSize = s;

    std::cout << "App Width : " << width << ", App Height : " << height << std::endl;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Error: Could not initalize SDL...";
        exit(1);
    }

    window = SDL_CreateWindow("2D Fluid Simulation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
    if (window == nullptr)
    {
        std::cout << "Error: could not create window...";
        exit(1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        std::cout << "Error: could not create renderer...";
        exit(1);
    }
}

App::~App()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void App::run()
{
    std::chrono::time_point<std::chrono::steady_clock> time, time_prev;
    std::chrono::duration<double, std::milli> diff;
    int fps;

    int i, j;
    int mousePositionX;
    int mousePositionY;

    bool holdingLeftClick = false;
    bool running = true;

    time_prev = std::chrono::steady_clock::now();
    SDL_GetMouseState(&mousePositionX, &mousePositionY);
    SDL_GetMouseState(&i, &j);

    float totalTime = 0.f;
    int nbFrames = 0;
    while (running)
    {
        time = std::chrono::steady_clock::now();
        diff = time - time_prev;
        double dt = diff.count();
        dt /= 1000.0; // Convert dt from ms to s
        totalTime += dt;
        if (duration > 0 && totalTime >= duration)
            break;
        nbFrames++;
        fps = static_cast<int>(1.0 / dt);

        SDL_GetMouseState(&i, &j);
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                running = false;
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
                running = false;
            else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
            {
                holdingLeftClick = true;
            }
            else if (event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_LEFT)
            {
                holdingLeftClick = false;
            }
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_SPACE && inputAllowed)
            {
                sim.save();
            }
            else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_RIGHT)
            {
                std::cout << "FPS: " << fps << std::endl;
            }
        }

        if (holdingLeftClick && inputAllowed)
        {
            // Add density when left clicking
            int start_x = i - 20 * 0.5;
            int start_y = j - 20 * 0.5;
            for (int x = 0; x < 20; ++x)
            {
                for (int y = 0; y < 20; ++y)
                {
                    int px = static_cast<int>((x + start_x) / particleSize);
                    int py = static_cast<int>((y + start_y) / particleSize);

                    if (px >= 0 && px < width && py >= 0 && py < height)
                        sim.addDensity(px, py, 8000.f * (1.f / float(particleSize)), dt);
                }
            }
        }

        if (inputAllowed)
        {
            // Add velocity when moving the mouse
            int start_x = i - 20 * 0.5;
            int start_y = j - 20 * 0.5;
            for (int x = 0; x < 20; ++x)
            {
                for (int y = 0; y < 20; ++y)
                {
                    int px = static_cast<int>((x + start_x) / particleSize);
                    int py = static_cast<int>((y + start_y) / particleSize);
                    int du = i - mousePositionX;
                    int dv = j - mousePositionY;
                    if (px >= 0 && px < width && py >= 0 && py < height)
                    {
                        float fact = 3 * (1.f / float(particleSize)) * (1.f / dt);
                        sim.addVelocity(px, py, du * fact, dv * fact, dt);
                    }
                }
            }
        }

        sim.step(dt);
        drawDensity();

        std::swap(mousePositionX, i);
        std::swap(mousePositionY, j);
        std::swap(time, time_prev);
    }
    std::cout << "Performances for " << totalTime << "s of simulation: " << totalTime / float(nbFrames) << "s per frame, or " 
              << 1 / (totalTime / float(nbFrames)) << " fps on avereage" << std::endl;
}

void App::drawDensity()
{
    SDL_RenderClear(renderer);
    SDL_Rect particle;

    for (int x = 0; x < width; x += particleSize)
    {
        for (int y = 0; y < height; y += particleSize)
        {
            particle.x = x;
            particle.y = y;
            particle.h = particleSize;
            particle.w = particleSize;

            // Get density from simulation
            int dens = sim.getDensity(x / particleSize, y / particleSize);
            dens = dens > 255 ? 255 : dens;
            SDL_SetRenderDrawColor(renderer, dens, dens, dens, 255);
            SDL_RenderFillRect(renderer, &particle);
        }
    }
    SDL_RenderPresent(renderer);
}