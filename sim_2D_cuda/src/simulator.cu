#include "simulator.cuh"
#include <utility>
#include <iostream>
#include <cassert>
#include <fstream>
#include <sstream>
#include "libs/helper_cuda.h"

#include "physics.cuh"

int Simulator::offset(int x, int y)
{
    return x + y * (width + 2);
}

Simulator::Simulator(uint blockSize, int width, int height, float scale, float visc, float diff)
    : width(width),
      height(height),
      visc(visc),
      diff(diff),
      scale(scale),
      size((width + 2) * (height + 2)),
      blockSize(blockSize)
{
    assert(width == height);

    std::cout << "array size : " << size << std::endl;
    std::cout << "simulator width : " << width << std::endl;
    checkCudaErrors(cudaMalloc((void **)&vx, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vx_prev, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy_prev, size * sizeof(float)));

    // Density
    checkCudaErrors(cudaMalloc((void **)&dens, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&dens_prev, size * sizeof(float)));

    dens_interface = new float[size];
    vx_interface = new float[size];
    vy_interface = new float[size];

    float *init = new float[size];

    for (int x = 0; x < size; ++x)
    {
        init[x] = 0.f;
        dens_interface[x] = 0.f;
        vx_interface[x] = 0.f;
        vy_interface[x] = 0.f;
    }
    checkCudaErrors(cudaMemcpy(vx, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vx_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    delete[] init;

    Physics::setWidth<<<1, 1>>>(width);
    Physics::setScale<<<1, 1>>>(scale);
}

Simulator::Simulator(uint blockSize, const std::string &fileName)
    : blockSize(blockSize)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open())
    {
        std::cout << "Error: could not open file" << std::endl;
        exit(1);
    }
    int s;
    file >> width >> height >> s;
    file >> visc >> diff;

    scale = 1.f / float(s);
    size = (width + 2) * (height + 2);
    height = width;

    assert(width == height);

    checkCudaErrors(cudaMalloc((void **)&vx, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vx_prev, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&vy_prev, size * sizeof(float)));

    // Density
    checkCudaErrors(cudaMalloc((void **)&dens, size * sizeof(float)));
    checkCudaErrors(cudaMalloc((void **)&dens_prev, size * sizeof(float)));

    dens_interface = new float[size];
    vx_interface = new float[size];
    vy_interface = new float[size];

    float *init = new float[size];

    for (int x = 0; x < size; ++x)
    {
        init[x] = 0.f;
    }

    // Get input from file
    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file >> dens_interface[i];
    }

    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file >> vx_interface[i];
    }

    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file >> vy_interface[i];
    }

    checkCudaErrors(cudaMemcpy(vx, vx_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vx_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy, vy_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens, dens_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(dens_prev, init, size * sizeof(float), cudaMemcpyHostToDevice));
    delete[] init;

    Physics::setWidth<<<1, 1>>>(width);
    Physics::setScale<<<1, 1>>>(scale);
}

Simulator::~Simulator()
{
    delete[] dens_interface;
    delete[] vx_interface;
    delete[] vy_interface;
    checkCudaErrors(cudaFree((void *)vx));
    checkCudaErrors(cudaFree((void *)vx_prev));
    checkCudaErrors(cudaFree((void *)vy));
    checkCudaErrors(cudaFree((void *)vy_prev));
    checkCudaErrors(cudaFree((void *)dens));
    checkCudaErrors(cudaFree((void *)dens_prev));
}

float Simulator::getDensity(int x, int y)
{
    return dens_interface[offset(x + 1, y + 1)];
}

void Simulator::addDensity(int x, int y, float densValue, float dt)
{
    dens_interface[offset(x, y)] += densValue * dt;
    dens_interface[offset(x, y)] /= 1.0f + dt;
}

void Simulator::addVelocity(int x, int y, float uValue, float vValue, float dt)
{
    vx_interface[offset(x, y)] += uValue * dt;
    vx_interface[offset(x, y)] /= 1.0f + dt;
    vy_interface[offset(x, y)] += vValue * dt;
    vy_interface[offset(x, y)] /= 1.0f + dt;
}

void Simulator::step(double dt)
{
    checkCudaErrors(cudaMemcpy(dens, dens_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vx, vx_interface, size * sizeof(float), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(vy, vy_interface, size * sizeof(float), cudaMemcpyHostToDevice));

    // Kernel arguments
    void *args[] = {
        (void *)&dt,
        (void *)&vx,
        (void *)&vy,
        (void *)&vx_prev,
        (void *)&vy_prev,
        (void *)&dens,
        (void *)&dens_prev,
        (void *)&visc,
        (void *)&diff,
    };

    // Grid and block dimentsions
    // const unsigned int blockSize = 32;
    dim3 blockDim(blockSize, blockSize, 1);
    unsigned int gridSize = (width + 2) % blockSize == 0 ? (width + 2) / blockSize : (width + 2) / blockSize + 1;

    // Check available executions nb
    int numBlocksPerSm = 0;
    cudaDeviceProp deviceProp;
    checkCudaErrors(cudaGetDeviceProperties(&deviceProp, 0));
    checkCudaErrors(cudaOccupancyMaxActiveBlocksPerMultiprocessor(&numBlocksPerSm, Physics::cudaStep, blockSize * blockSize, 0));
    uint maxExec = deviceProp.multiProcessorCount * numBlocksPerSm;

    if (gridSize * gridSize > maxExec)
    {
        gridSize = floor(sqrt(maxExec));
    }
    // std::cout << "Max Executions in Grid: " << maxExec << std::endl;

    dim3 gridDim(gridSize, gridSize, 1);

    // Launch kernel
    checkCudaErrors(cudaLaunchCooperativeKernel((void *)Physics::cudaStep, gridDim, blockDim, args));
    checkCudaErrors(cudaDeviceSynchronize());

    checkCudaErrors(cudaMemcpy(dens_interface, dens, size * sizeof(float), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(vx_interface, vx, size * sizeof(float), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(vy_interface, vy, size * sizeof(float), cudaMemcpyDeviceToHost));
}

void Simulator::save() const
{
    std::ofstream file;
    std::stringstream ss;
    std::string fileName;

    int s = int(1.f / scale);

    ss << "../data/" << width * s << "-" << s << ".txt";
    ss >> fileName;

    file.open(fileName);
    if (!file.is_open())
    {
        std::cout << "Error: could not open file" << std::endl;
        exit(1);
    }

    file << width << " " << height << " " << s << std::endl;
    file << visc << " " << diff << std::endl;
    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file << dens_interface[i] << " ";
    }
    file << std::endl;
    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file << vx_interface[i] << " ";
    }
    file << std::endl;
    for (int i = 0; i < (width + 2) * (height + 2); ++i)
    {
        file << vy_interface[i] << " ";
    }
    file << std::endl;

    file.close();
}