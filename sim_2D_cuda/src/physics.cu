#include "physics.cuh"

#include <utility>
#include <cooperative_groups.h>

__device__ int Physics::offset(int x, int y)
{
    return x + y * (width + 2);
}

// Can't use swap or move in CUDA...
__device__ void Physics::swap(float *&pf1, float *&pf2)
{
    float *temp;
    temp = std::move(pf1);
    pf1 = std::move(pf2);
    pf2 = std::move(temp);
}

__device__ int Physics::getIndexX()
{
    return blockDim.x * blockIdx.x + threadIdx.x;
}

__device__ int Physics::getIndexY()
{
    return blockDim.y * blockIdx.y + threadIdx.y;
}

__global__ void Physics::setWidth(int w)
{
    width = w;
}

__global__ void Physics::setScale(float s)
{
    scale = s;
}

/**
 * @brief Solve a system of linear equations using the Gauss-Seidel solver
 *
 * @param b Boundary conditions settings
 * @param x Output
 * @param x0 Initial guess
 * @param a Linear system parameter (multiplication)
 * @param c Linear system parameter (division)
 */
__device__ void Physics::lin_solve(boundaryType b, float *x, float *x0, float a, float c)
{
    uint totalThreadCount = gridDim.x * blockDim.x;

    for (int k = 0; k < 20; ++k)
    {
        for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
        {
            for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
            {
                if (i > 0 && i <= width && j > 0 && j <= width)
                {
                    x[offset(i, j)] = (x0[offset(i, j)] + a * (x[offset(i - 1, j)] + x[offset(i + 1, j)] + x[offset(i, j - 1)] + x[offset(i, j + 1)])) / c;
                }
            }
        }

        cooperative_groups::this_grid().sync();

        setBoundaries(b, x);

        cooperative_groups::this_grid().sync();
    }
}

// Sets the boundary cells
__device__ void Physics::setBoundaries(boundaryType b, float *x)
{
    uint totalThreadCount = gridDim.x * blockDim.x;

    if (b == boundaryType::reboundVertical)
    {
        for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
        {
            for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
            {
                if (j > 0 && j <= width)
                {
                    if (i == 0)
                        x[offset(0, j)] = -x[offset(1, j)];
                    if (i == width + 1)
                        x[offset(width + 1, j)] = -x[offset(width, j)];
                }
            }
        }
    }
    else
    {
        for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
        {
            for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
            {
                if (j > 0 && j <= width)
                {
                    if (i == 0)
                        x[offset(0, j)] = x[offset(1, j)];
                    if (i == width + 1)
                        x[offset(width + 1, j)] = x[offset(width, j)];
                }
            }
        }
    }

    if (b == boundaryType::reboundHorizontal)
    {
        for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
        {
            for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
            {
                if (i > 0 && i <= width)
                {
                    if (j == 0)
                        x[offset(i, 0)] = -x[offset(i, 1)];
                    if (j == width + 1)
                        x[offset(i, width + 1)] = -x[offset(i, width)];
                }
            }
        }
    }
    else
    {
        for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
        {
            for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
            {
                if (i > 0 && i <= width)
                {
                    if (j == 0)
                        x[offset(i, 0)] = x[offset(i, 1)];
                    if (j == width + 1)
                        x[offset(i, width + 1)] = x[offset(i, width)];
                }
            }
        }
    }

    // Handle the 4 corners
    for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
    {
        for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
        {
            if (i == 0 && j == 0)
                x[offset(0, 0)] = 0.5 * (x[offset(1, 0)] + x[offset(0, 1)]);
            if (i == 0 && j == width + 1)
                x[offset(0, width + 1)] = 0.5 * (x[offset(1, width + 1)] + x[offset(0, width)]);
            if (i == width + 1 && j == 0)
                x[offset(width + 1, 0)] = 0.5 * (x[offset(width, 0)] + x[offset(width + 1, 1)]);
            if (i == width + 1 && j == width + 1)
                x[offset(width + 1, width + 1)] = 0.5 * (x[offset(width, width + 1)] + x[offset(width + 1, width)]);
        }
    }
}

/**
 * @brief Diffusion of x0
 *
 * @param b Boundary condition settings
 * @param x Output
 * @param x0 Quantity to be diffused
 * @param diff Diffusion rate
 * @param dt Time step
 */
__device__ void Physics::diffuse(boundaryType b, float *x, float *x0, float diff, float dt)
{
    float a = dt * diff * scale * scale;
    lin_solve(b, x, x0, a, 1 + 4 * a);
}

/**
 * @brief Move d0 along the velocity field
 *
 * @param b Boundary conditions settings
 * @param d Output
 * @param d0 Quantity to advect
 * @param vx x Component of the velocity field
 * @param vy y Component of the velocity field
 * @param dt Time step
 */
__device__ void Physics::advect(boundaryType b, float *d, float *d0, float *vx, float *vy, float dt)
{
    int i0, j0, i1, j1;
    float x, y, s0, t0, s1, t1;

    float dt0 = dt * scale;

    uint totalThreadCount = gridDim.x * blockDim.x;
    for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
    {
        for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
        {
            if (i > 0 && i <= width && j > 0 && j <= width)
            {
                // Trace the cell velocity backwards in time
                x = i - dt0 * vx[offset(i, j)];
                y = j - dt0 * vy[offset(i, j)];

                // Stay in the boundaries
                if (x < 0.5)
                    x = 0.5;
                if (x > width + 0.5)
                    x = width + 0.5;
                if (y < 0.5)
                    y = 0.5;
                if (y > width + 0.5)
                    y = width + 0.5;

                // Linear interpolation over the previous density values to find the current one
                i0 = static_cast<int>(x);
                i1 = i0 + 1;
                j0 = static_cast<int>(y);
                j1 = j0 + 1;

                s1 = x - i0;
                s0 = 1 - s1;
                t1 = y - j0;
                t0 = 1 - t1;

                d[offset(i, j)] = s0 * (t0 * d0[offset(i0, j0)] + t1 * d0[offset(i0, j1)]) +
                                  s1 * (t0 * d0[offset(i1, j0)] + t1 * d0[offset(i1, j1)]);
            }
        }
    }

    cooperative_groups::this_grid().sync();
    setBoundaries(b, d);
    cooperative_groups::this_grid().sync();
}

/**
 * @brief Forces the velocities to be mass-conserving
 *
 * Idea: every velocity field is the sum of an incompressible field and a gradient field.
 *      (Hodge decomposition). We want to substract the gradient field to the velocity field
 *      in order to make the velocity field incompressible.
 *
 * @param vx
 * @param vy
 * @param p
 * @param div
 * @return __device__
 */
__device__ void Physics::project(float *vx, float *vy, float *p, float *div)
{
    float h = 1.0 / scale;

    uint totalThreadCount = gridDim.x * blockDim.x;

    // Calculating the divergence div of the velocity field
    // For an incompressible fluid, it should be 0 everywhere,
    // but it is not the case here (for now).
    for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
    {
        for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
        {
            if (i > 0 && i <= width && j > 0 && j <= width)
            {
                div[offset(i, j)] = -0.5 * h * (vx[offset(i + 1, j)] - vx[offset(i - 1, j)] + vy[offset(i, j + 1)] - vy[offset(i, j - 1)]);
                p[offset(i, j)] = 0.0;
            }
        }
    }

    // Set boundaries
    cooperative_groups::this_grid().sync();
    setBoundaries(boundaryType::noRebound, div);
    setBoundaries(boundaryType::noRebound, p);
    cooperative_groups::this_grid().sync();

    // Solve Poisson equation to get the scalar/height field (= pressure field)
    lin_solve(boundaryType::noRebound, p, div, 1, 4);

    // The gradient of the pressure field found by solving the Poisson Equation
    // is substracted from the velocity field :
    // Only the divergence-free component remain !
    for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
    {
        for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
        {
            if (i > 0 && i <= width && j > 0 && j <= width)
            {
                vx[offset(i, j)] -= 0.5 * (p[offset(i + 1, j)] - p[offset(i - 1, j)]) / h;
                vy[offset(i, j)] -= 0.5 * (p[offset(i, j + 1)] - p[offset(i, j - 1)]) / h;
            }
        }
    }

    // Set boundaries
    cooperative_groups::this_grid().sync();
    setBoundaries(boundaryType::reboundVertical, vx);
    setBoundaries(boundaryType::reboundHorizontal, vy);
    cooperative_groups::this_grid().sync();
}

// Density solver
// Input: dens_prev[] contains the source densities
// Output: dens[]
__device__ void Physics::dens_step(float *dens, float *dens_prev, float *vx, float *vy, float diff, float dt)
{
    swap(dens_prev, dens);
    diffuse(boundaryType::noRebound, dens, dens_prev, diff, dt);
    swap(dens_prev, dens);
    advect(boundaryType::noRebound, dens, dens_prev, vx, vy, dt);
}

// Velocity solver
// Input: vx_prev[] and vy_prev[] contain the source velocities
// Output: vx[] and vy[]
__device__ void Physics::vel_step(float *vx, float *vy, float *vx_prev, float *vy_prev, float visc, float dt)
{
    swap(vx_prev, vx);
    swap(vy_prev, vy);
    // Viscous diffusion
    diffuse(boundaryType::reboundVertical, vx, vx_prev, visc, dt);
    diffuse(boundaryType::reboundHorizontal, vy, vy_prev, visc, dt);
    project(vx, vy, vx_prev, vy_prev);
    swap(vx_prev, vx);
    swap(vy_prev, vy);
    // Self-advection of the velocity field
    advect(boundaryType::reboundVertical, vx, vx_prev, vx_prev, vy_prev, dt);
    advect(boundaryType::reboundHorizontal, vy, vy_prev, vx_prev, vy_prev, dt);
    project(vx, vy, vx_prev, vy_prev);
}

__device__ void Physics::fade_density(float *dens, float dt)
{
    uint totalThreadCount = gridDim.x * blockDim.x;
    for (uint i = getIndexX(); i < width + 2; i += totalThreadCount)
    {
        for (uint j = getIndexY(); j < width + 2; j += totalThreadCount)
        {
            float d = dens[offset(i, j)];
            dens[offset(i, j)] = (d - 0.03f < 0) ? 0 : d - 0.03f;
        }
    }
}

__global__ void Physics::cudaStep(double dt, float *vx, float *vy,
                                  float *vx_prev, float *vy_prev,
                                  float *dens, float *dens_prev,
                                  float visc, float diff)
{
    int i = getIndexX();
    int j = getIndexY();
    if (i > width + 1 || j > width + 1)
        return;

    // Run simulation
    vel_step(vx, vy, vx_prev, vy_prev, visc, dt);
    dens_step(dens, dens_prev, vx, vy, diff, dt);

    // Fade density (for better visual result)
    // fade_density(dens, dt);
}