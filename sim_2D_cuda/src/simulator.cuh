#ifndef __SIMULATOR_H__
#define __SIMULATOR_H__


#include <string>

class Simulator
{
public:
    Simulator(uint blockSize, int width, int height, float scale, float visc = 0.001, float diff = 300.f);
    Simulator(uint blockSize, const std::string& fileName);
    ~Simulator();

    void step(double dt);

    void addDensity(int x, int y, float dens, float dt);
    void addVelocity(int x, int y, float vx, float vy, float dt);

    float getDensity(int x, int y);

    void save() const;

private:
    // Mapping a 2D array to a 1D array
    int offset(int x, int y);

private:
    int width;
    int height;
    int size; // Total array size

    // Velocity
    float *vx; // Device memory
    float *vy; // Device memory

    float *vx_prev; // Device memory
    float *vy_prev; // Device memory

    // Density
    float *dens;      // Device memory
    float *dens_prev; // Device memory

    float *dens_interface; // Host memory
    float *vx_interface;   // Host memory
    float *vy_interface;   // Host memory

    
    float scale;
    float visc;
    float diff;

    const uint blockSize;
};

#endif // __SIMULATOR_H__
