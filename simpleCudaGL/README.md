# Credit

This code is based on [cuda2GLcore](https://github.com/Forceflow/cuda2GLcore/), which is based on the *SimpleCuda2GL* sample provided in the [CUDA Samples by Nvidia](http://docs.nvidia.com/cuda/cuda-samples/index.html).
