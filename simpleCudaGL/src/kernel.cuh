#include "cuda_runtime.h"

__device__ float clamp(float x, float a, float b);

__device__ int rgbToInt(float r, float g, float b);

__global__ void
cudaRender(unsigned int *g_odata, int imgw);

void launch_cudaRender(dim3 grid, dim3 block, int sbytes, unsigned int *g_odata, int imgw);