#ifndef __APP_HPP__
#define __APP_HPP__

#include "cudaGLRenderer.hpp"

class App : public CudaGLRenderer
{
public:
    App(uint width = 256, uint height = 256);

    void launchCuda();
};

#endif // __APP_HPP__
