#ifndef __CUDAGLRENDERER_HPP__
#define __CUDAGLRENDERER_HPP__

#include <SDL2/SDL.h>
#include <GL/glew.h>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "shader.hpp"

class CudaGLRenderer
{
public:
    CudaGLRenderer(uint width = 256, uint height = 256);
    ~CudaGLRenderer();

    void run();

private:
    void initSDL();
    void initGL();

    void createGLTextureForCUDA(GLuint *gl_tex, cudaGraphicsResource **cuda_tex,
                                unsigned int size_x, unsigned int size_y);
    void initCUDABuffers();

    void setupGL();

    void generateCudaImage();
    virtual void launchCuda() = 0;

    void display();
   
protected:
    const uint width;
    const uint height;

    SDL_Window *window;
    SDL_GLContext glContext;

    Shader shaderProgram;

    void *cuda_dev_render_buffer; // Cuda buffer for initial render
    struct cudaGraphicsResource *cuda_tex_resource;
    GLuint opengl_tex_cuda; // OpenGL Texture for cuda result

    GLuint vao;
    GLuint vbo;
    GLuint ebo;
};

#endif // __CUDAGLRENDERER_HPP__
