#include "app.hpp"

#include "kernel.cuh"

App::App(uint width, uint height)
    : CudaGLRenderer(width, height)
{
}

void App::launchCuda()
{
    // calculate grid size
    dim3 block(16, 16, 1);
    dim3 grid(CudaGLRenderer::width / block.x, CudaGLRenderer::height / block.y, 1);                  // 2D grid, every thread will compute a pixel
    launch_cudaRender(grid, block, 0, (unsigned int *)CudaGLRenderer::cuda_dev_render_buffer, width); // launch with 0 additional shared memory allocated
}