# ===== Variables =====
# Executable
OUT = main.out

# Sources
SRC_DIR = src/
CUDA_SRC = $(wildcard $(SRC_DIR)*.cu)
CPP_SRC = $(shell find $(SRC_DIR) -type f -name *.cpp)

# Object files
OBJ = obj/
CUDA_OBJ_LIST = $(addprefix $(OBJ), $(patsubst %.cu, %.cu.o, $(CUDA_SRC:$(SRC_DIR)%=%)))
CPP_OBJ_LIST = $(addprefix $(OBJ), $(patsubst %.cpp, %.o, $(CPP_SRC:$(SRC_DIR)%=%))) 
# $(CPP_SRC:$(SRC_DIR)%=%)))
OBJ_LIST = $(CUDA_OBJ_LIST) $(CPP_OBJ_LIST)

# Compiler flags
CFLAGS = $(shell pkg-config sdl2 --cflags) -Ihelpers -I/usr/local/cuda/include

# Linker flags
CUDALIBS = -L"/usr/local/cuda/targets/x86_64-linux/lib/stubs" -L"/usr/local/cuda/targets/x86_64-linux/lib" -lcudadevrt -lcudart_static -lrt -lpthread -ldl
LDFLAGS = $(CUDALIBS) -lGLEW -lGL -lGLU -lglut -lglfw $(shell pkg-config sdl2 --libs)

# ===== Targets =====
.PHONY: init default clean

default: init $(OUT)

# Setup
init: $(OBJ)

$(OBJ):
	mkdir $(OBJ)

# Dependency generation
DEP := $(patsubst %.o, %.d, $(OBJ_LIST))
-include $(DEP)
DEPFLAGS = -MMD -MF $(@:.o=.d)

# Linker
$(OUT): $(OBJ_LIST)
	g++  -g $+ -o $@ $(LDFLAGS)

# Cuda compilation
$(OBJ)%.cu.o: $(SRC_DIR)%.cu
	nvcc -g -x cu -c $< -o $@ $(DEPFLAGS) $(CFLAGS) 

# Cpp compilation
$(OBJ)%.o: $(SRC_DIR)%.cpp
	mkdir -p $(dir $@)
	g++ -g -Wall -Wno-deprecated-declarations -c $< -o $@ $(DEPFLAGS) $(CFLAGS)


# === Utility ===
clean:
	rm -rf $(OBJ) $(OUT)

# host memcheck
memcheck-host: default
	valgrind ./$(OUT)

# device memcheck
memcheck: default
	compute-sanitizer --leak-check full ./$(OUT)

